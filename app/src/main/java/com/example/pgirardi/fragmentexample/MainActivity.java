package com.example.pgirardi.fragmentexample;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.pgirardi.fragmentexample.fragments.ArticleFragment;
import com.example.pgirardi.fragmentexample.fragments.HeadlinesFragment;

public class MainActivity extends Activity implements HeadlinesFragment.OnHeadlineSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_articles);

        //checking if the activity is using container (small layout).
        if(findViewById(R.id.fragment_container) != null){

            //if the instance is already created, no need created the transaction again.
            if(savedInstanceState != null)
                return;

            //fragment to be placed in the activity layout
            HeadlinesFragment headlinesFragment = new HeadlinesFragment();

            // In case this activity was started by an Intent... than take the extras of intent
            // and pass to arguments to the fragment. (if you need you can use)
            headlinesFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container'  (container = FrameLayout)
            getFragmentManager().beginTransaction().add(R.id.fragment_container, headlinesFragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onArticleSelected(int position) {

        ArticleFragment artFrag = (ArticleFragment) getFragmentManager().findFragmentById(R.id.article_fragment);

        if(artFrag != null){
            artFrag.updateArticleView(position);
        }else{
            ArticleFragment articleFragment = new ArticleFragment();

            Bundle args = new Bundle();
            args.putInt(ArticleFragment.ARG_POSITION, position);
            articleFragment.setArguments(args);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, articleFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
